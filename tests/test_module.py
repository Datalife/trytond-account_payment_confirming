# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountPaymentConfirmingTestCase(ModuleTestCase):
    """Test Account Payment Confirming module"""
    module = 'account_payment_confirming'
    extras = ['account_payment_sepa', 'account_payment_sepa_es']


del ModuleTestCase
